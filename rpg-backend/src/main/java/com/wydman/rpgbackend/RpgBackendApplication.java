package com.wydman.rpgbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RpgBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(RpgBackendApplication.class, args);
	}

}
